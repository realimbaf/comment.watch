import {API_ROOT} from "../_constants/config";
import {pidChannel} from "../_constants/comment";

export const commentService = {
    getComments,
    addComment
};

function getComments({pid = pidChannel, offset = 1,count = 10}) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    return fetch(`${API_ROOT}marta/api/v1/?pid=${pid}&offset=${offset}&count=${count}`, requestOptions).then(handleResponse);
}
function addComment({comment = ""}){

    const requestOptions = {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body : JSON.stringify({comment_text : comment,channel_post : pidChannel})
    };
    return fetch(`${API_ROOT}marta/api/v1/`, requestOptions).then(handleResponse);
}


function handleResponse(response) {
    if (!response.ok) {
        return Promise.reject(response.statusText);
    }
    return response.json();
}