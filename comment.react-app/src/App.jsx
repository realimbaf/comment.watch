import React, {Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import CommentLayout from './Layouts/CommentLayout';
import CommentsIndex from "./Comments/commentsIndex";
import TelegramIndex from "./Telegram/telegramIndex";

class App extends Component {
    render() {
        return (
            <div>
                <CommentLayout>
                    <CommentsIndex/>
                     <TelegramIndex/>
                </CommentLayout>
            </div>

    );
    }
    }

    export default App;
