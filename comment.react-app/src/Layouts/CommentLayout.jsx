import React, {Component} from 'react';
import Footer from "../components/Footer/Footer";
class CommentLayout extends Component {
    render() {
        return (
            <div>
                <div className="top-bar-right main-menu"></div>
                <div className="container-fluid sg_group_page">
                    <div className="row">
                        <div className="col-lg-10 center-block sg_group_post_wrap">
                            <div id="post" className="sg_gp_post">
                                <div className="sg_gp_post_text">Представляем
                                    <a href="http://t.me/MartaBot" target="_blank" rel="noopener noreferrer"> t.me/MartaBot </a>
                                    — публикация постов с комментариями в удобном виде.
                                </div>
                                <div className="sg_gp_post_panel clearfix">
                                    <a href="https://t.me/MartaBot?start=open_post_BkUvTFTuM" target="_blank">March 07, 2018</a>
                                </div>
                            </div>
                            {this.props.children}
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>

        );
    }
}

export default CommentLayout;
