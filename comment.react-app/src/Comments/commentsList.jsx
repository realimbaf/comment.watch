import React from 'react';
import CommentsSingle from "./commentSingle";
import {chunkComment} from "../_constants/comment";

class CommentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showAll : false
        }
    }

    onToggleShow = () => {
        this.setState({
            showAll : !this.state.showAll
        })
    }
    render(){
        const {showAll} = this.state;
        const {comments} = this.props;
        const commentLength = comments.length;
        const showCount = showAll ? (commentLength) : (commentLength <= chunkComment ? commentLength : chunkComment) ;
        return (
                <div id="comments" className="sg_gp_comments_wrap">
                    <div className="sg_gp_block_comments no_radius">
                        <div className="sg_load">{`Show ${showCount} comments out of ${commentLength}`}</div>
                        {commentLength > chunkComment &&
                            <div className="sg_load" onClick={this.onToggleShow}>{showAll ? `Hide to ${showCount}...` : 'Show all...'}</div>
                        }
                        <div className="cb-comments-wrap">
                            {comments.slice(0,showAll ? commentLength : chunkComment).map(comment=>{
                                return <CommentsSingle key={comment.id} comment={comment}/>
                            })}
                        </div>
                        {commentLength == 0 &&
                            <div className="sg_gp_block_comments no_radius">
                                <div className="sg_gp_block_no_comments">No comments here yet...</div>
                            </div>
                        }
                    </div>
                </div>
        );
    }
}


export default CommentsList;