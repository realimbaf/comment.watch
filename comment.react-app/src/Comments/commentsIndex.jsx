import React from 'react';
import CommentsList from './commentsList';
import CommentAdd from "./commentAdd";
import {commentService} from "../_services/commentService";

class CommentsIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comments : [],
        }
        this.onAdd = this.onAdd.bind(this);
    }
    async componentDidMount() {
        const comments =  await commentService.getComments({count : 20});
        this.setState({
            comments
        })
    }
    async onAdd(comment){
        try {
            const result =  await commentService.addComment({comment});
            this.setState({
                comments : [...this.state.comments,result[0]]
            })
        }catch (e){
            console.log(e);
        }
    }
    render(){
        const {comments} = this.state;
        return (
            <div>
                <CommentsList comments={comments}/>
                <CommentAdd onAdd={this.onAdd}/>
            </div>
        );
    }
}

export default CommentsIndex;