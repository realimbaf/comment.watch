import React from 'react';
import {formatDate} from "../_helpers/utils";
import userIcon from '../_assets/images/icon.png';

class CommentsSingle extends React.Component {

    render(){
        const {comment:{user,comment_text,created_at}} = this.props;

        const getUserName = (user) => {
            if(!user.first_name || user.first_name === '-' && !user.last_name || user.last_name === '-')
                return 'Anonymous';
            return ` ${user.last_name} ${user.first_name}`;
        }
        return (
                <div className="sg_gp_comment clearfix">
                    <div>
                        <div className="sg_gp_comment_avatar pull-left">
                            <img src={userIcon} alt="User icon" />
                        </div>
                        <div className="sg_gp_comment_body clearfix">
                            <div className="sg_gp_comment_user">{getUserName(user)}</div>
                            <div className="sg_gp_comment_text">{comment_text}
                            </div>
                            <div className="sg_gp_comment_panel clearfix">
                                <div className="pull-right">

                                </div>
                                <span className="sg_gp_comment_date">{formatDate(created_at)}</span>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}


export default CommentsSingle;