import React from 'react';

class CommentAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment : ''
        }
    }
    onChange = e => {
        this.setState({
            comment : e.target.value
        })
    }

    render(){
        const {comment} = this.state;
        const {onAdd} = this.props;
        return (
            <div className="sg_gp_comment clearfix">
                <div className="sg_gp_comments_wrap sg_gp_post" id="add-comment-area">
                      <textarea name="text" rows="2" placeholder="Write a comment..."
                                className="textarea comment_area" onChange={this.onChange} value={comment}>
                      </textarea>
                        <div className="add-panel">
                            <button type="button" onClick={(e)=> {onAdd(comment)}} className="sg-btn">Send</button>
                            <span className="sg_auth_info">as Anonymous
                                <a href="#">
                                    <span id="username"></span>
                                </a>
                            </span>
                        </div>
                </div>
            </div>
        );
    }
}


export default CommentAdd;