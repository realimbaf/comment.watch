import React from 'react';
import TelegramLogin from "../components/TelegramLogin";

export default class TelegramIndex extends React.Component {
    render(){
        return (
            <div className="sg_gp_comments_wrap">
                <div className="sg_gp_block_comments no_radius" id="login-button-area">
                    <div className="sg_gp_block_no_comments">Do you want to add a new comment as an authorized user?</div>
                    <div className="center_margin">
                        <div>
                            <TelegramLogin dataOnauth={(x=>{console.log(x)})} botName="comro2_bot"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
