export function formatDate(value) {
    const date = new Date(value);
    var year = date.getFullYear(),
        month = date.getMonth() + 1,
        day = date.getDate(),
        hour = date.getHours(),
        minute = date.getMinutes(),
        hourFormatted = hour % 12 || 12,
        minuteFormatted = minute < 10 ? "0" + minute : minute,
        morning = hour < 12 ? "am" : "pm";

    return month + "." + day + "." + year + " " + hourFormatted + ":" +
        minuteFormatted + morning;
}