
import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <div id="footer" className="sg_footer">
                <div className="cb-created-via">
                    Created via <a href="/" target="_blank">Marta</a>
                    <sup className="beta">Beta</sup>
                </div>
                <a href="//t.me/MartaChannel" id="cb-channel-link" target="_blank">Our channel</a>
                <span className="cb-footer-dot">·</span><a href="//t.me/Marta" target="_blank">Support</a>
            </div>
        );
    }
}

export default Footer;

